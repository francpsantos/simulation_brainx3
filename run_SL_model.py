import numpy as np
from numba import njit
import WBM as WBM
import scipy.signal as sig
from pymatreader import read_mat
import os


def run_SL_model(K, a_bif, md, simulation_time, signal_type):

  '''
  Runs simulation of the SL model for the combination of free parameters specified as arguments
  ands stores MEG signals, BOLD signals, or both.

  Args:
    K (float): global coupling, scaling factor for structural connectivity
    a_bif (float): bifurcation parameter of the SL model. a_bif<0 represents a subcritical state, while a_bif>0 represents supercritical dynamics 
    md (float): mean conduction delay in ms. This is used with the loaded connection lengths to compute conduction speed. Values between 1ms and 15ms are ok.
    simulation_time (float): simulation time to be stored, in ms. Actual simulation time will be longer to give the model time to stabilize after initialization.
                             The script will only store the last simulation_time points of the simulation.
    signal_type (str): type of data to be stored. Options: 'MEG', 'fMRI'. 
                       If MEG is selected, avoid sim_time longer than 1 minute, because time-resolution is high. 
                       If fMRI is selected, avoid sim_shorter than 10 minutes, because fluctuations are slow. 
                       Also, for fMRI, 5 minutes of signal will be removed in the end due to boundary effects of the ballon-windkessel model.
  '''

  # Loading connectivity information
  mat = read_mat('Data/Connectivity/SC_90aal_32HCP.mat')

  # Structural connectivity
  connect = mat['mat']
  connect[connect < 10] = 0 # Removes negligible connections with less than 10 fibers
  np.fill_diagonal(connect, 0) # Ensures there are no self connections
  connect /= np.max(connect) # Normalizes connectivity matrix so that maximum value is 1

  # Tract lengths
  t_len = mat['mat_D']  

  # Picks either only cortical areas or all areas. This only works for the AAL90 atlas!!!
  cortical = True
  if cortical:
      mask = np.concatenate((np.arange(36),
                             np.array([38, 39]),
                             np.arange(42, 70),
                             np.arange(78, 90)))
              
      connect = connect[:, mask][mask]
      t_len = t_len[:, mask][mask]

  # Computes conduction velocity from md parameter and tract lengths
  if md == 0:
      speed = -10 # If speed is negative, WBM functions recognize this as md = 0. This was done because conduction velocity cannot be infinite
  else:
      speed = np.mean(t_len[connect>0])/md # [connect > 0] ensures that we only account for connectioms between nodes that are actually connected


  #################
  ## Compilation ##
  #################

  # This step just runs the functions once with a short simulation time so that numba compiles them before running the actual simulation.
  # This should only take a few seconds
  w_test = connect.copy()
  test_sig = WBM.simulate_SL(t_len = t_len, 
                             W_mat = w_test, 
                             sim_time = 1000, 
                             store_time = 1000, 
                             dt = 1, 
                             dt_save = 1, 
                             a_bif = 0, 
                             speed = speed)

  test = WBM.balloon_windkessel(np.real(test_sig), dt = 1)
  print('Compilation done!')


  ################
  ## Simulation ##
  ################

  # Simulation parameters
  store_time = simulation_time # Total time to be recorded, in ms
  if signal_type == 'MEG':
    sim_time = simulation_time + 10*1e3 # Maximum simulation time, in ms
  elif signal_type == 'fMRI':
    sim_time = simulation_time + 60*1e3 # Maximum simulation time, in ms

  dt = 0.2 # ms. Integration time step of simulations
  dt_save = 1 # ms. Time step to store model results, we chose 1ms to improve tractability

  # Scales structural connectivity
  W = K*connect.copy()

  # Runs simulation
  print('#####################################')
  print('############Simulating...############')
  print('#####################################')
  print('K = {} || a = {} || md = {}'.format(np.around(K, 2), 
                                             np.around(a_bif, 2), 
                                             md))

  output = WBM.simulate_SL(t_len = t_len,
                           W_mat = W, 
                           sim_time = sim_time, 
                           store_time = store_time, 
                           dt = dt, 
                           dt_save = dt_save, 
                           a_bif = a_bif, 
                           speed = speed)

  signal_raw = np.real(output) # Gets real part of output. This is essential for the SL model, which function in complex space.

  if signal_type == 'MEG':

      # Resamples MEG signal to 250 Hz to reduce size of data
      frs = 250
      fs = 1e3/dt_save
      n_samples = int(frs * (signal_raw.shape[-1]) / fs)
      signal_MEG = np.zeros((signal_raw.shape[0], n_samples))
      for n in range(signal_raw.shape[0]):
          signal_MEG[n, :] = sig.resample(signal_raw[n, :], n_samples)

      os.makedirs('Data/Results', exist_ok = True)

      np.save(r"Data/Results/MEG_K{}_a{}_md{}.npy".format(np.around(K, 2), 
                                                          np.around(a_bif, 2),
                                                          md), signal_MEG)


  elif signal_type == 'fMRI':

      # Runs signal through Balloon-Windkessel model. This is a forward model that generates a BOLD signal from model activity.
      signal = WBM.balloon_windkessel(signal_raw, dt_save)

      # Resamples BOLD signal to a sampling frequency typical of empirical BOLD recordings (in this case, the HCP dataset)
      frs = 1/(0.72)
      fs = 1e3/dt_save
      nrs = int(fs/frs)
      bdr = int(2.5 * 60 * fs) # Time to remove at the beginning and end of signal, to avoid boundary effects from the Balloon-Windkessel model

      n_samples = int(frs * (signal.shape[-1] - 2 * bdr) / fs)
      signal_MRI = np.zeros((signal.shape[0], n_samples))

      for n in range(signal.shape[0]):
          signal_MRI[n, :] = sig.resample(signal[n, bdr:-bdr], n_samples)

      os.makedirs('Data/Results', exist_ok = True)

      np.save(r"Data/Results/BOLD_K{}_a{}_md{}.npy".format(np.around(K, 2), 
                                                           np.around(a_bif, 2),
                                                           md), signal_MRI)